import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Callback from "./components/Callback/Callback";
import Login from "./components/Login/Login";
import Home from "./components/Home/Home";
import Search from "./components/Search/Search";
import Playlists from "./components/Playlists/Playlists";
import NotFound from "./components/NotFound/NotFoud";
import Profile from "./components/Profile/Profile";
import Artist from "./components/Artist/Artist";
import SpotPlayer from "./components/SpotPlayer/SpotPlayer";

import { Provider } from "react-redux";
import { store, persistor } from "./helpers/store";
import { PersistGate } from "redux-persist/lib/integration/react";

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/callback" component={Callback} />
            <Route exact path="/home" component={Home} />
            <Route exact path="/search" component={Search} />
            <Route exact path="/playlists" component={Playlists} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/artist/:artistID" component={Artist} />
            <Route path="*" exact={true} component={NotFound} />
          </Switch>
          <SpotPlayer />
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
