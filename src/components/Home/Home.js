import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import Menu from "../Menu/Menu";
import { getTopTracks } from "../../redux/services/spotServices";
import Card from "../../components/Card/Card";
import "./Home.css";

function Home() {
  const userReducer = useSelector((state) => state.userReducer);
  const spotReducer = useSelector((state) => state.spotReducer);
  const dispatch = useDispatch();

  const [playedOne, setPlayedOne] = useState(-1);
  const [playedTwo, setPlayedTwo] = useState(-1);

  useEffect(() => {
    dispatch(getTopTracks(userReducer.user.access_token));
  }, [userReducer, dispatch]);

  return (
    <div className="outerWrap">
      <div className="App">
        <Menu />
        <div className="main">
          <h1>Accueil</h1>
          <h2>Écoutés récemment</h2>

          <div className="Recently">
            {spotReducer.requesting_recently_played ? (
              <div>Loading...</div>
            ) : (
              spotReducer.recently_played.map((item, index) => (
                <Card
                  key={index}
                  index={index}
                  setPlayed={setPlayedOne}
                  isPlaying={index === playedOne ? true : false}
                  token={userReducer.user.access_token}
                  image={item.track.album.images[0].url}
                  name={item.track.name}
                  artist={item.track.artists[0].name}
                  uri={item.track.uri}
                  artist_id={item.track.artists[0].id}
                />
              ))
            )}
          </div>

          <h2>Vos favoris cette année</h2>

          <div className="Recently">
            {spotReducer.requesting_top_tracks ? (
              <div>Loading...</div>
            ) : (
              spotReducer.top_tracks.map((item, index) => (
                <Card
                  key={index}
                  index={index}
                  setPlayed={setPlayedTwo}
                  isPlaying={index === playedTwo ? true : false}
                  token={userReducer.user.access_token}
                  image={item.album.images[0].url}
                  name={item.name}
                  artist={item.artists[0].name}
                  uri={item.uri}
                  artist_id={item.artists[0].id}
                />
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
