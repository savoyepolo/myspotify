import React from "react";
import "./Menu.css";
import SpotifyLogo from "../SpotifyLogo/SpotifyLogo";
import { NavLink } from "react-router-dom";
import { logOut } from "../../redux/actions/userActions";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { Redirect } from "react-router";

function Menu() {
  const dispatch = useDispatch();
  const userReducer = useSelector((state) => state.userReducer);

  return (
    <header className="Menu">
      {!userReducer.loggedIn && <Redirect to="/" />}
      <div className="Menu_title">
        <SpotifyLogo className="Menu_logo" />
        <h3>MySpotify</h3>
      </div>
      <div className="Menu_list">
        <NavLink className="Menu_items" to="/home" activeClassName="active">
          <img src={require("../../assets/images/home.png")} alt="" />
          <div className="Menu_title">Accueil</div>
        </NavLink>
        <NavLink className="Menu_items" to="/search">
          <img src={require("../../assets/images/search.png")} alt="" />
          <div className="Menu_title">Rechercher</div>
        </NavLink>
        <NavLink className="Menu_items" to="/playlists">
          <img src={require("../../assets/images/playlist.png")} alt="" />
          <div className="Menu_title">Playlists</div>
        </NavLink>
        <NavLink className="Menu_items" to="/profile">
          <img src={require("../../assets/images/profile.png")} alt="" />
          <div className="Menu_title">Profil</div>
        </NavLink>
      </div>
      <div className="Menu_disconnect" onClick={() => dispatch(logOut())}>
        Déconnecter
      </div>
    </header>
  );
}

export default Menu;
