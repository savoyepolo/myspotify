import React, { useState } from "react";
import { useSelector } from "react-redux";
import Menu from "../Menu/Menu";
import "./Search.css";
import { getSearch } from "../../redux/services/searchServices";
import { useDispatch } from "react-redux";
import Card from "../Card/Card";

function Search() {
  const userReducer = useSelector((state) => state.userReducer);
  const searchReducer = useSelector((state) => state.searchReducer);
  const [playedOne, setPlayedOne] = useState(-1);
  const [playedTwo, setPlayedTwo] = useState(-1);

  const dispatch = useDispatch();

  var timer;

  console.log(searchReducer);

  return (
    <div className="outerWrap">
      <div className="App">
        <Menu />
        <div className="main">
          <h1>Rechercher</h1>
          <div className="Search">
            <form>
              <label>
                <input
                  onChange={(e) => {
                    clearTimeout(timer);
                    timer = setTimeout(() => {
                      dispatch(
                        getSearch(userReducer.user.access_token, e.target.value)
                      );
                    }, 500);
                  }}
                  type="text"
                  name="name"
                />
              </label>
            </form>
            {searchReducer.search_result && (
              <div className="Search">
                <h2>Titres</h2>
                <div className="Search_box">
                  {searchReducer.search_result.tracks.items.map(
                    (item, index) => (
                      <Card
                        key={index}
                        index={index}
                        setPlayed={setPlayedOne}
                        isPlaying={index === playedOne ? true : false}
                        token={userReducer.user.access_token}
                        image={item.album.images[0].url}
                        name={item.name}
                        artist={item.artists[0].name}
                        uri={item.uri}
                        artist_id={item.artists[0].id}
                      />
                    )
                  )}
                </div>
                <h2>Artistes</h2>

                <div className="Search_box">
                  {searchReducer.search_result.artists.items.map(
                    (item, index) => (
                      <Card
                        key={index}
                        index={index}
                        setPlayed={setPlayedTwo}
                        isPlaying={index === playedTwo ? true : false}
                        token={userReducer.user.access_token}
                        image={
                          item.images.length > 0
                            ? item.images[0].url
                            : require("../../assets/images/man.png")
                        }
                        name={item.name}
                        artist={item.name}
                        contextUri={item.uri}
                        artist_id={item.id}
                        isArtist
                      />
                    )
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Search;
