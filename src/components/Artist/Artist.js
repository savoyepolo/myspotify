import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";

import Menu from "../Menu/Menu";
import Tracks from "../Tracks/Tracks";
import {
  getArtistInfo,
  getArtistTopTracks,
} from "../../redux/services/artistServices";
import "./Artist.css";

function numberWithSpaces(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function Profile() {
  const artistReducer = useSelector((state) => state.artistReducer);
  const userReducer = useSelector((state) => state.userReducer);
  const dispatch = useDispatch();

  const { artistID } = useParams();

  useEffect(() => {
    dispatch(getArtistInfo(userReducer.user.access_token, artistID));
    dispatch(getArtistTopTracks(userReducer.user.access_token, artistID));
  }, [dispatch, artistID, userReducer.user.access_token]);

  return (
    <div className="outerWrap">
      <div className="App">
        <Menu />
        <div className="main">
          <h1>Artiste</h1>
          {artistReducer.requesting_artist ||
          artistReducer.requesting_top_tracks ? (
            <div>Loading...</div>
          ) : (
            <div>
              <div className="Artist">
                <img alt="" src={artistReducer.artist_info.images[0].url} />
                <div className="Artist_text">
                  <div className="Artist_username">
                    {artistReducer.artist_info.name}
                  </div>
                  <div>
                    {numberWithSpaces(
                      artistReducer.artist_info.followers.total
                    )}{" "}
                    abonnés
                  </div>
                </div>
              </div>
              <h2>Titre les plus écoutés</h2>
              <Tracks
                tracks={artistReducer.artist_top_tracks}
                token={userReducer.user.access_token}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default Profile;
