import React, {useEffect} from 'react';
import queryString from 'query-string'
import { useSelector } from "react-redux";
import { useLocation } from 'react-router'
import { fetchUser } from '../../redux/services/authServices'
import { useDispatch } from 'react-redux'
import { Redirect } from "react-router";

function Callback() {

  const dispatch = useDispatch()
  const location = useLocation();

  const user = useSelector((state) => state.userReducer);



    useEffect(() => {
      const search = queryString.parse(location.search)
        dispatch(fetchUser(search.code));
      }, [dispatch, location]);
    
    return (
      <div>
        {user.loggedIn &&
          <Redirect to="/home" />
        }
      </div>
    )
}

export default Callback;