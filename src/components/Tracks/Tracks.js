import React from "react";
import "./Tracks.css";
import { startUserPlayback } from "../../redux/services/spotServices";

const zeroPad = (num, places) => String(num).padStart(places, "0");

function msToTime(s) {
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;

  return mins + ":" + zeroPad(secs, 2);
}

function Tracks(props) {
  return (
    <div className="Tracks">
      {props.tracks.map((track, index) => {
        return (
          <div
            key={index}
            onClick={() => {
              startUserPlayback(props.token, track.uri);
            }}
            className="Track"
          >
            <div className="Track_index">{index + 1}</div>
            <img
              className="Track_image"
              src={track.album.images[2].url}
              alt=""
            />
            <div className="Track_title">{track.name}</div>
            {track.explicit && (
              <img
                className="Track_explicit"
                src={require("../../assets/images/explicit.png")}
                alt=""
              />
            )}
            <div className="Track_duration">{msToTime(track.duration_ms)}</div>
          </div>
        );
      })}
    </div>
  );
}

export default Tracks;
