import React, { useEffect } from "react";
import "./SpotPlayer.css";
import "rc-slider/assets/index.css";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  getRecentlyPlayed,
  getCurrentPlayback,
} from "../../redux/services/spotServices";
import { useLocation } from "react-router-dom";
import SpotifyPlayer from "react-spotify-web-playback";

function SpotPlayer() {
  const userReducer = useSelector((state) => state.userReducer);
  const spotReducer = useSelector((state) => state.spotReducer);
  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    dispatch(getCurrentPlayback(userReducer.user.access_token));
    dispatch(getRecentlyPlayed(userReducer.user.access_token));
  }, [dispatch, userReducer]);

  console.log(spotReducer);

  return (
    <div className="Spotplayer">
      <div className="Spotplayer_content">
        {!spotReducer.requesting_recently_played &&
          !spotReducer.requesting_current_playback &&
          location.pathname !== "/" &&
          userReducer.user.access_token && (
            <SpotifyPlayer
              styles={{
                bgColor: "#282828",
                color: "#fff",
                loaderColor: "#fff",
                sliderColor: "#00bcd4",
                savedColor: "#fff",
                trackArtistColor: "#ccc",
                trackNameColor: "#fff",
                sliderHandleColor: "white",
              }}
              token={userReducer.user.access_token}
              uris={
                spotReducer.current_playback
                  ? [spotReducer.current_playback.item.uri]
                  : [spotReducer.recently_played[0].track.uri]
              }
              autoPlay={true}
              name="MySpotify Player"
              showSaveIcon
            />
          )}
      </div>
    </div>
  );
}

export default SpotPlayer;
