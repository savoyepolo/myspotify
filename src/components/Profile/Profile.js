import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import Menu from "../Menu/Menu";
import { getCurrentUserProfile } from "../../redux/services/profileServices";
import "./Profile.css";
import ReactCountryFlag from "react-country-flag";

function Profile() {
  const profileReducer = useSelector((state) => state.profileReducer);
  const userReducer = useSelector((state) => state.userReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCurrentUserProfile(userReducer.user.access_token));
  }, [dispatch, userReducer.user.access_token]);

  return (
    <div className="outerWrap">
      <div className="App">
        <Menu />
        <div className="main">
          <h1>Profil</h1>
          {profileReducer.requesting ? (
            <div>Loading...</div>
          ) : (
            <div className="Profile">
              <img alt="" src={profileReducer.user_profile.images[0].url} />
              <div className="Profile_text">
                <div>PROFIL</div>
                <div className="Profile_username">
                  {profileReducer.user_profile.display_name}
                </div>
                <ReactCountryFlag
                  countryCode={profileReducer.user_profile.country}
                  style={{
                    fontSize: "2em",
                  }}
                  svg
                />
                <div>{profileReducer.user_profile.followers.total} abonnés</div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default Profile;
