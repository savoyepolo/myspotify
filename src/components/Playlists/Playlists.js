import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getPlaylistCurrentUser } from "../../redux/services/playlistServices";
import Menu from "../Menu/Menu";
import Card from "../../components/Card/Card";
import "./Playlist.css";

function Playlists() {
  const userReducer = useSelector((state) => state.userReducer);
  const playlistReducer = useSelector((state) => state.playlistReducer);
  const dispatch = useDispatch();

  const [played, setPlayed] = useState(-1);

  useEffect(() => {
    dispatch(getPlaylistCurrentUser(userReducer.user.access_token));
  }, [dispatch, userReducer.user.access_token]);

  return (
    <div className="outerWrap">
      <div className="App">
        <Menu />
        <div className="main">
          <h1>Playlists</h1>
          <div className="Playlist">
            {playlistReducer.requesting ? (
              <div>Loading...</div>
            ) : (
              playlistReducer.playlist_current_user.map((item, index) => (
                <Card
                  key={index}
                  index={index}
                  setPlayed={setPlayed}
                  isPlaying={index === played ? true : false}
                  token={userReducer.user.access_token}
                  image={item.images[0].url}
                  name={item.name}
                  owner={item.owner.display_name}
                  contextUri={item.uri}
                />
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Playlists;
