import React, { useEffect } from "react";
import queryString from "query-string";
import "./Login.css";
import { useDispatch } from "react-redux";
import { autoLogin } from "../../redux/services/authServices";
import { useSelector } from "react-redux";
import { Redirect } from "react-router";
import SpotifyLogo from "../SpotifyLogo/SpotifyLogo";

export const client_id = "599e2115eaa44a9cb36418c44f106156";
export const client_secret = "883de1618bfb49178593af1ff01cb9db";
const scope =
  "user-read-private user-read-email user-read-recently-played user-read-playback-state streaming user-modify-playback-state user-library-read user-library-modify playlist-read-private playlist-read-collaborative user-top-read";
export const redirect_uri = "http://localhost:3000/callback";

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function getURL() {
  return (
    "https://accounts.spotify.com/authorize?" +
    queryString.stringify({
      response_type: "code",
      client_id: client_id,
      scope: scope,
      redirect_uri: redirect_uri,
      state: makeid(43),
    })
  );
}

function Home() {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.userReducer);

  useEffect(() => {
    var refresh_token = localStorage.getItem("token");
    if (refresh_token) {
      dispatch(autoLogin());
    }
  }, [dispatch]);

  return (
    <div className="App_login">
      <div className="login">
        <SpotifyLogo />
        <h1>MySpotify App</h1>
        <a className="login_button" href={getURL()}>
          Login with Spotify
        </a>
      </div>
      {user.loggedIn && <Redirect to="/home" />}
      <div className="App_credits">Paul Savoye - Jessy Beltra</div>
    </div>
  );
}

export default Home;
