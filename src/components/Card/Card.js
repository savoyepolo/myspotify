import React, { useState } from "react";
import {
  startUserPlayback,
  pauseUserPlayback,
} from "../../redux/services/spotServices";
import "./Card.css";
import { Link } from "react-router-dom";

function Card(props) {
  const [hover, setHover] = useState(false);

  var cardimage = props.isArtist ? "Card_image artist" : "Card_image";

  return (
    <div className="Card">
      <img
        onMouseEnter={() => setHover(true)}
        alt=""
        className="Card_player"
        style={{ opacity: hover ? 1 : 0 }}
        src={
          props.isPlaying
            ? require("../../assets/images/pause.png")
            : require("../../assets/images/play.png")
        }
      />
      <img
        onClick={() => {
          if (props.isPlaying) {
            props.setPlayed(-1);
            pauseUserPlayback(props.token);
          } else {
            props.setPlayed(props.index);
            startUserPlayback(props.token, props.uri, props.contextUri);
          }
        }}
        alt=""
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        className={cardimage}
        style={{ opacity: hover ? 0.15 : 1 }}
        src={props.image}
      />
      <div className="Card_title">{props.name}</div>

      {props.artist ? (
        <Link to={"/artist/" + props.artist_id}>{props.artist}</Link>
      ) : (
        <div className="Card_owner">{props.owner}</div>
      )}
    </div>
  );
}

export default Card;
