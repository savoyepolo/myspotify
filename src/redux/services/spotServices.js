import {
  recently_played_request,
  recently_played_success,
  current_playback_request,
  current_playback_success,
  current_playback_failure,
  top_tracks_request,
  top_tracks_success,
} from "../actions/spotActions";

export const getRecentlyPlayed = (token) => (dispatch) => {
  dispatch(recently_played_request());
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  fetch(
    "https://api.spotify.com/v1/me/player/recently-played?limit=30",
    requestOptions
  )
    .then((res) => res.json())
    .then((data) => {
      if (!data.error)
        dispatch(
          recently_played_success(
            data.items.filter(
              (v, i, a) => a.findIndex((t) => t.track.uri === v.track.uri) === i
            )
          )
        );
    });
};

export const getCurrentPlayback = (token) => (dispatch) => {
  dispatch(current_playback_request());
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };

  fetch("https://api.spotify.com/v1/me/player", requestOptions)
    .then((res) => {
      if (res.status === 200) return res.json();
      else if (res.status === 204) return undefined;
      else throw Error(res);
    })
    .then((data) => {
      dispatch(current_playback_success(data));
    })
    .catch((error) => {
      dispatch(current_playback_failure(error));
    });
};

export const getTopTracks = (token) => (dispatch) => {
  dispatch(top_tracks_request());
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };

  fetch(
    " https://api.spotify.com/v1/me/top/tracks?time_range=medium_term&limit=18",
    requestOptions
  )
    .then((res) => res.json())
    .then((data) => {
      dispatch(top_tracks_success(data.items));
    });
};

export const startUserPlayback = (token, uris, contextUri) => {
  var body = {};

  if (uris) {
    body = JSON.stringify({
      uris: [uris],
    });
  } else
    body = JSON.stringify({
      context_uri: contextUri,
    });
  const requestOptions = {
    method: "PUT",
    headers: {
      Authorization: "Bearer " + token,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: body,
  };

  fetch("https://api.spotify.com/v1/me/player/play", requestOptions)
    .then((res) => {
      console.log(res);
    })
    .catch((error) => {
      console.log(error);
    });
};

export const pauseUserPlayback = (token) => {
  const requestOptions = {
    method: "PUT",
    headers: {
      Authorization: "Bearer " + token,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };

  fetch("https://api.spotify.com/v1/me/player/pause", requestOptions)
    .then((res) => {
      console.log(res);
    })
    .catch((error) => {
      console.log(error);
    });
};
