import {
  search_request,
  search_success,
  search_failure,
} from "../actions/searchActions";

export const getSearch = (token, query) => (dispatch) => {
  if (query !== "") {
    dispatch(search_request());
    const requestOptions = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
      },
    };

    query = query.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    query = query.replace(/\s+/g, "+");

    fetch(
      "https://api.spotify.com/v1/search?type=track%2Cartist&limit=10&q=" +
        query,
      requestOptions
    )
      .then((res) => res.json())
      .then((data) => {
        dispatch(search_success(data));
      })
      .catch((error) => {
        dispatch(search_failure(error));
      });
  }
};
