import {
  playlist_current_user_request,
  playlist_current_user_success,
  playlist_current_user_failure,
} from "../actions/playlistActions";

export const getPlaylistCurrentUser = (token) => (dispatch) => {
  dispatch(playlist_current_user_request());
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  fetch("https://api.spotify.com/v1/me/playlists", requestOptions)
    .then((res) => res.json())
    .then((data) => {
      dispatch(playlist_current_user_success(data.items));
    })
    .catch((error) => {
      dispatch(playlist_current_user_failure(error));
    });
};
