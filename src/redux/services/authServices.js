import { logIn } from "../actions/userActions";
import {
  redirect_uri,
  client_id,
  client_secret,
} from "../../components/Login/Login";
import { formBody } from "../../lib/formBody";

const ah = btoa(client_id + ":" + client_secret);

export const fetchUser = (code) => (dispatch) => {
  const body = {
    grant_type: "authorization_code",
    code: code,
    redirect_uri: redirect_uri,
  };

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: "Basic " + ah,
    },
    body: formBody(body),
  };

  fetch("https://accounts.spotify.com/api/token", requestOptions)
    .then((res) => res.json())
    .then((data) => {
      localStorage.setItem("token", data.refresh_token);
      dispatch(logIn(data));
    });
};

export const autoLogin = () => (dispatch) => {
  const body = {
    grant_type: "refresh_token",
    refresh_token: localStorage.getItem("token"),
  };

  fetch("https://accounts.spotify.com/api/token", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: "Basic " + ah,
    },
    body: formBody(body),
  })
    .then((res) => {
      if (!res.ok) {
        throw res;
      }
      return res.json();
    })
    .then((data) => dispatch(logIn(data)));
};
