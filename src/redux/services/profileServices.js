import {
  user_profile_request,
  user_profile_success,
} from "../actions/profileActions";

export const getCurrentUserProfile = (token) => (dispatch) => {
  dispatch(user_profile_request());
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  fetch("https://api.spotify.com/v1/me", requestOptions)
    .then((res) => res.json())
    .then((data) => {
      dispatch(user_profile_success(data));
    });
};
