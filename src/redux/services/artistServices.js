import {
  artist_info_request,
  artist_info_success,
  artist_top_tracks_request,
  artist_top_tracks_success,
} from "../actions/artistActions";

export const getArtistInfo = (token, id) => (dispatch) => {
  dispatch(artist_info_request());
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  fetch("https://api.spotify.com/v1/artists/" + id, requestOptions)
    .then((res) => res.json())
    .then((data) => {
      dispatch(artist_info_success(data));
    });
};

export const getArtistTopTracks = (token, id) => (dispatch) => {
  dispatch(artist_top_tracks_request());
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  fetch(
    "https://api.spotify.com/v1/artists/" + id + "/top-tracks?country=FR",
    requestOptions
  )
    .then((res) => res.json())
    .then((data) => {
      dispatch(artist_top_tracks_success(data.tracks));
    });
};
