import { authConstants } from "../constants/constants";

export const logIn = (user) => ({ type: authConstants.LOG_IN, user });
export const logOut = () => ({ type: authConstants.LOG_OUT });
