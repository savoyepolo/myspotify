import { profileConstants } from "../constants/constants";

export const user_profile_request = () => ({
  type: profileConstants.GET_CURRENT_USER_PROFILE_REQUEST,
});

export const user_profile_success = (user_profile) => ({
  type: profileConstants.GET_CURRENT_USER_PROFILE_SUCCESS,
  user_profile,
});

export const user_profile_failure = (error) => ({
  type: profileConstants.GET_CURRENT_USER_PROFILE_FAILURE,
  error,
});
