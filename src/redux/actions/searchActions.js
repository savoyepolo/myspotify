import { searchConstants } from "../constants/constants";

export const search_request = () => ({
  type: searchConstants.GET_SEARCH_REQUEST,
});

export const search_success = (search_result) => ({
  type: searchConstants.GET_SEARCH_SUCCESS,
  search_result,
});

export const search_failure = (error) => ({
  type: searchConstants.GET_SEARCH_FAILURE,
  error,
});
