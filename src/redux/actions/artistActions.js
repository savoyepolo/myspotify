import { artistConstants } from "../constants/constants";

export const artist_info_request = () => ({
  type: artistConstants.GET_ARTIST_REQUEST,
});

export const artist_info_success = (artist_info) => ({
  type: artistConstants.GET_ARTIST_SUCCESS,
  artist_info,
});

export const artist_info_failure = (error) => ({
  type: artistConstants.GET_ARTIST_FAILURE,
  error,
});

export const artist_top_tracks_request = () => ({
  type: artistConstants.GET_ARTIST_TOP_TRACKS_REQUEST,
});

export const artist_top_tracks_success = (artist_top_tracks) => ({
  type: artistConstants.GET_ARTIST_TOP_TRACKS_SUCCESS,
  artist_top_tracks,
});

export const artist_top_tracks_failure = (error) => ({
  type: artistConstants.GET_ARTIST_TOP_TRACKS_FAILURE,
  error,
});
