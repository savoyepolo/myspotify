import { playlistConstants } from "../constants/constants";

export const playlist_current_user_request = () => ({
  type: playlistConstants.GET_CURRENT_USER_PLAYLIST_REQUEST,
});

export const playlist_current_user_success = (playlist_current_user) => ({
  type: playlistConstants.GET_CURRENT_USER_PLAYLIST_SUCCESS,
  playlist_current_user,
});

export const playlist_current_user_failure = (error) => ({
  type: playlistConstants.GET_CURRENT_USER_PLAYLIST_FAILURE,
  error,
});
