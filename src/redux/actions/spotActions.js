import { spotConstants } from "../constants/constants";

export const recently_played_request = () => ({
  type: spotConstants.GET_RECENTLY_PLAYED_REQUEST,
});

export const recently_played_success = (recently_played) => ({
  type: spotConstants.GET_RECENTLY_PLAYED_SUCCESS,
  recently_played,
});

export const recently_played_failure = (error) => ({
  type: spotConstants.GET_RECENTLY_PLAYED_FAILURE,
  error,
});

export const current_playback_request = () => ({
  type: spotConstants.GET_CURRENT_PLAYBACK_REQUEST,
});

export const current_playback_success = (current_playback) => ({
  type: spotConstants.GET_CURRENT_PLAYBACK_SUCCESS,
  current_playback,
});

export const current_playback_failure = (error) => ({
  type: spotConstants.GET_CURRENT_PLAYBACK_FAILURE,
  error,
});

export const top_tracks_request = () => ({
  type: spotConstants.GET_TOP_TRACKS_REQUEST,
});

export const top_tracks_success = (top_tracks) => ({
  type: spotConstants.GET_TOP_TRACKS_SUCCESS,
  top_tracks,
});

export const top_tracks_failure = (error) => ({
  type: spotConstants.GET_TOP_TRACKS_FAILURE,
  error,
});
