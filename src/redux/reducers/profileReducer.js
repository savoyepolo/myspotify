import { profileConstants } from "../constants/constants";

const initialState = {
  requesting: true,
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case profileConstants.GET_CURRENT_USER_PROFILE_REQUEST:
      return {
        ...state,
        requesting: true,
      };
    case profileConstants.GET_CURRENT_USER_PROFILE_SUCCESS:
      return {
        ...state,
        user_profile: action.user_profile,
        requesting: false,
      };
    case profileConstants.GET_CURRENT_USER_PROFILE_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
}

export default userReducer;
