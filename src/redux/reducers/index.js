import userReducer from "./userReducer";
import spotReducer from "./spotReducer";
import artistReducer from "./artistReducer";
import playlistReducer from "./playlistReducer";
import profileReducer from "./profileReducer";
import searchReducer from "./searchReducer";

import { combineReducers } from "redux";

const rootReducer = combineReducers({
  userReducer,
  spotReducer,
  artistReducer,
  playlistReducer,
  profileReducer,
  searchReducer,
});

export default rootReducer;
