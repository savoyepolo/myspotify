import { authConstants } from "../constants/constants";

const initialState = {
  loggedIn: false,
  user: {},
  user_profile: {},
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case authConstants.LOG_IN:
      return {
        ...state,
        loggedIn: true,
        user: action.user,
      };
    case authConstants.LOG_OUT:
      localStorage.clear();
      return {
        ...state,
        loggedIn: false,
        user: {},
      };
    default:
      return state;
  }
}

export default userReducer;
