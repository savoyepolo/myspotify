import { artistConstants } from "../constants/constants";

const initialState = { requesting_artist: true, requesting_top_tracks: true };

function artistReducer(state = initialState, action) {
  switch (action.type) {
    case artistConstants.GET_ARTIST_REQUEST:
      return {
        ...state,
        requesting_artist: true,
      };
    case artistConstants.GET_ARTIST_SUCCESS:
      return {
        ...state,
        artist_info: action.artist_info,
        requesting_artist: false,
      };
    case artistConstants.GET_ARTIST_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    case artistConstants.GET_ARTIST_TOP_TRACKS_REQUEST:
      return {
        ...state,
        requesting_top_tracks: true,
      };
    case artistConstants.GET_ARTIST_TOP_TRACKS_SUCCESS:
      return {
        ...state,
        requesting_top_tracks: false,
        artist_top_tracks: action.artist_top_tracks,
      };
    case artistConstants.GET_ARTIST_TOP_TRACKS_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
}

export default artistReducer;
