import { searchConstants } from "../constants/constants";

const initialState = {
  requesting: true,
};

function searchReducer(state = initialState, action) {
  switch (action.type) {
    case searchConstants.GET_SEARCH_REQUEST:
      return {
        ...state,
        requesting: true,
      };
    case searchConstants.GET_SEARCH_SUCCESS:
      return {
        ...state,
        search_result: action.search_result,
        requesting: false,
      };
    case searchConstants.GET_SEARCH_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
}

export default searchReducer;
