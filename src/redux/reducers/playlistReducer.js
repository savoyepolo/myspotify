import { playlistConstants } from "../constants/constants";

const initialState = { requesting: true };

function playlistReducer(state = initialState, action) {
  switch (action.type) {
    case playlistConstants.GET_CURRENT_USER_PLAYLIST_REQUEST:
      return {
        ...state,
        requesting: true,
      };
    case playlistConstants.GET_CURRENT_USER_PLAYLIST_SUCCESS:
      return {
        ...state,
        requesting: false,
        playlist_current_user: action.playlist_current_user,
      };
    case playlistConstants.GET_CURRENT_USER_PLAYLIST_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
}

export default playlistReducer;
