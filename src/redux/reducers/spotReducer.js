import { spotConstants } from "../constants/constants";

const initialState = {
  requesting_recently_played: true,
  requesting_current_playback: true,
  requesting_top_tracks: true,
};

function spotReducer(state = initialState, action) {
  switch (action.type) {
    case spotConstants.GET_RECENTLY_PLAYED_REQUEST:
      return {
        ...state,
        requesting_recently_played: true,
      };
    case spotConstants.GET_RECENTLY_PLAYED_SUCCESS:
      return {
        ...state,
        recently_played: action.recently_played,
        requesting_recently_played: false,
      };
    case spotConstants.GET_RECENTLY_PLAYED_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    case spotConstants.GET_CURRENT_PLAYBACK_REQUEST:
      return {
        ...state,
        requesting_current_playback: true,
      };
    case spotConstants.GET_CURRENT_PLAYBACK_SUCCESS:
      return {
        ...state,
        current_playback: action.current_playback,
        requesting_current_playback: false,
      };
    case spotConstants.GET_CURRENT_PLAYBACK_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    case spotConstants.GET_TOP_TRACKS_REQUEST:
      return {
        ...state,
        requesting_top_tracks: true,
      };
    case spotConstants.GET_TOP_TRACKS_SUCCESS:
      return {
        ...state,
        top_tracks: action.top_tracks,
        requesting_top_tracks: false,
      };
    case spotConstants.GET_TOP_TRACKS_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
}

export default spotReducer;
