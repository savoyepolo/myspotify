import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import rootReducer from "../redux/reducers/index";
const persistConfig = {
  key: "spotify",
  storage: storage,
  whitelist: ["userReducer"],
};

const pReducer = persistReducer(persistConfig, rootReducer);
const middleware = applyMiddleware(thunk);
export const store = createStore(pReducer, middleware);
export const persistor = persistStore(store);
