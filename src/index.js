import React from "react";
import "./index.css";
import { render } from "react-dom";
import App from "./App";

let rootElement = document.getElementById("root");

render(<App />, rootElement);
